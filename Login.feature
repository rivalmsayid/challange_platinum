Feature: Login
    Background: The user access registration page
        Given The user is already on the registration page
        When Enter the page https://secondhand.binaracademy.org/
        And Click the Sign in button
        And Click Register here

    @positive_case
    Scenario: TC.Reg.001.001-The user has successfully registered with valid information
        When Fill in the registration form with valid information such as name, email address and password
        And Click the Register button
        Then The user has successfully registered and redirected to the main page https://secondhand.binaracademy.org/

    @negative_case
    Scenario Outline: TC.Reg.001.002-The user failed to register because he entered the wrong email format
        When Fill in the registration form with the wrong email format. email content as rockgmail.com without @ sign
        And Fill in password
        And Click the Register button
        Then An error message appears indicating that the email format entered must include @ in the email address
        And Users cannot continue the registration process until all required fields are filled in with the correct format

    @negative_case
    Scenario: TC.Reg.001.003-The user failed to register because he entered an email that was already registered
        When Fill in the registration form with valid information, including a pre-registered email address
        And Fill in password
        And Click the Register button
        Then An error message appears indicating that the email address entered was previously registered
        And Users cannot continue the registration process until an unregistered email address is entered
       